<?php

/**
 * @file
 * DOI registration for Serial Article Work 2.0 provided by mEDRA.
 *
 * Date: 21 Aug 2019 10:16:21
 * File: medra_crossref_submit.admin.inc
 * Author: stefano.
 */

/**
 * Implements drupal_get_form().
 *
 * SC 21 Aug 2019 10:18:58 stefano.
 */
function _medra_crossref_submit_admin_settings($form, &$form_state) {
  // XML.
  //
  // SC 19 lug 2018 18:17:05 stefano.
  //
  $form['medra_crossref_submit_xml'] = array(
    '#type' => 'fieldset',
    '#title' => t('XML Tags'),
    '#description' => t('The XML static values.'),
    '#weight' => 1,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['medra_crossref_submit_xml']['medra_crossref_submit_journal'] = array(
    '#type' => 'fieldset',
    '#title' => t('Citation information about the journal issue'),
    '#weight' => 3,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['medra_crossref_submit_xml']['medra_crossref_submit_journal']['medra_crossref_submit_fromcompany'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('From company'),
    '#description' => t('Name of the organization performing the DOI registration.'),
    '#default_value' => variable_get('medra_crossref_submit_fromcompany', ''),
    '#weight' => 4,
    '#required' => TRUE,
  );
  $form['medra_crossref_submit_xml']['medra_crossref_submit_journal']['medra_crossref_submit_fromperson'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('From person'),
    '#description' => t('Name of the person performing the DOI registration.'),
    '#default_value' => variable_get('medra_crossref_submit_fromperson', ''),
    '#weight' => 5,
    '#required' => FALSE,
  );
  $form['medra_crossref_submit_xml']['medra_crossref_submit_journal']['medra_crossref_submit_fromemail'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('From email'),
    '#description' => t('The value in the FromEmail element will be used by the system to notify the feedback on the outcome of registration message submission.'),
    '#default_value' => variable_get('medra_crossref_submit_fromemail', ''),
    '#weight' => 6,
    '#required' => TRUE,
  );

  $form['medra_crossref_submit_xml']['medra_crossref_submit_doi_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('DOI prefix'),
    '#description' => t('The DOI <strong>prefix</strong> (the part before the slash [/], excluded) assigned to the organization performing the DOI registration, in the form “<strong>prefix</strong>/suffix". Example: <strong>10.0000</strong>/abcd.'),
    '#default_value' => variable_get('medra_crossref_submit_doi_prefix', ''),
    '#weight' => 7,
    '#required' => TRUE,
  );

  $form['medra_crossref_submit_xml']['medra_crossref_submit_issn'] = array(
    '#type' => 'textfield',
    '#title' => t('ISSN'),
    '#description' => t('The ISSN assigned to the organization performing the DOI registration.'),
    '#default_value' => variable_get('medra_crossref_submit_issn', ''),
    '#weight' => 8,
    '#required' => FALSE,
  );

  $form['medra_crossref_submit_xml']['medra_crossref_submit_notification_response'] = array(
    '#type' => 'select',
    '#title' => t('Notification response'),
    '#description' => t('For every registration message, please specify the chosen mode for the interaction with the mEDRA registration system. mEDRA will use the chosen mode for the notification of the feedback on the outcome of registration message submission.'),
    '#default_value' => variable_get('medra_crossref_submit_notification_response', ''),
    '#weight' => 9,
    '#required' => TRUE,
    '#options' => array(
      '01' => 'e-mail',
      '02' => 'callback',
      /* '03' => 'ftp', */
    ),
    '#multiple' => FALSE,
  );

  // FTP.
  //
  // SC 19 lug 2018 18:17:43 stefano.
  //
  /* $form['medra_crossref_submit_ftp'] = array(
  '#type' => 'fieldset',
  '#title' => t('FTP account'),
  '#description' => t('Contact publisher@ncbi.nlm.nih.gov to request FTP login information.'),
  '#weight' => 2,
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
  );
  $form['medra_crossref_submit_ftp']['medra_crossref_submit_username'] = array(
  '#type' => 'textfield',
  '#size' => 50,
  '#maxlength' => 256,
  '#title' => t('Username'),
  '#description' => t('Your login name.'),
  '#default_value' => variable_get('medra_crossref_submit_username', ''),
  '#required' => TRUE,
  );
  $form['medra_crossref_submit_ftp']['medra_crossref_submit_password'] = array(
  '#type' => 'textfield',
  '#size' => 50,
  '#maxlength' => 256,
  '#title' => t('Password'),
  '#description' => t('Your password.'),
  '#default_value' => variable_get('medra_crossref_submit_password', ''),
  '#required' => TRUE,
  );
  $form['medra_crossref_submit_ftp']['medra_crossref_submit_url'] = array(
  '#type' => 'textfield',
  '#size' => 50,
  '#maxlength' => 256,
  '#title' => t('URL'),
  '#description' => t('The ftp URL. The URL is absolute (beginning with a scheme such as "ftp:").'),
  '#default_value' => variable_get('medra_crossref_submit_url', ''),
  '#required' => TRUE,
  ); */

  // Node.
  //
  // SC 19 lug 2018 18:18:07 stefano.
  //
  $form['medra_crossref_submit_node'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node'),
    '#description' => t('Options related to node.'),
    '#weight' => 3,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['medra_crossref_submit_node']['medra_crossref_submit_ntype'] = array(
    '#type' => 'select',
    '#title' => t('Content type'),
    '#options' => node_type_get_names(),
    '#description' => t('The manuscript content type ready for mEDRA/Crossref DOI.'),
    '#default_value' => variable_get('medra_crossref_submit_ntype', array()),
    '#multiple' => TRUE,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 99,
  );

  return $form;
}

/**
 * Implements Form API submit.
 *
 * SC 30 Aug 2019 17:20:30 stefano.
 */
function _medra_crossref_submit_admin_settings_submit($form, &$form_state) {
  /* variable_set('medra_crossref_submit_xsd_file', $form_state['values']['medra_crossref_submit_xsd_file']);. */
  variable_set('medra_crossref_submit_fromcompany', $form_state['values']['medra_crossref_submit_fromcompany']);
  variable_set('medra_crossref_submit_fromperson', $form_state['values']['medra_crossref_submit_fromperson']);
  variable_set('medra_crossref_submit_fromemail', $form_state['values']['medra_crossref_submit_fromemail']);
  variable_set('medra_crossref_submit_doi_prefix', $form_state['values']['medra_crossref_submit_doi_prefix']);
  variable_set('medra_crossref_submit_issn', $form_state['values']['medra_crossref_submit_issn']);
  variable_set('medra_crossref_submit_notification_response', $form_state['values']['medra_crossref_submit_notification_response']);

  // FTP.
  //
  // SC 19 lug 2018 18:18:48 stefano.
  //
  /* variable_set('medra_crossref_submit_username', $form_state['values']['medra_crossref_submit_username']); */
  /* variable_set('medra_crossref_submit_password', $form_state['values']['medra_crossref_submit_password']); */
  /* variable_set('medra_crossref_submit_url', $form_state['values']['medra_crossref_submit_url']); */

  // Node.
  //
  // SC 19 lug 2018 18:18:58 stefano.
  //
  variable_set('medra_crossref_submit_ntype', $form_state['values']['medra_crossref_submit_ntype']);

}

/**
 * Implements Form API validate.
 *
 * SC 30 Aug 2019 17:21:21 stefano.
 */
function _medra_crossref_submit_admin_settings_validate($form, &$form_state) {

  // If there is the ISSN, string consisting of: 4 digits + dash (optional) + 3
  // digits + 1 digit or the X character.
  if (!empty($form_state['values']['medra_crossref_submit_issn'])) {
    if (!preg_match('/\d{4,4}-?\d{3,3}[0-9xX]/', $form_state['values']['medra_crossref_submit_issn'])) {
      form_set_error('medra_crossref_submit_issn', t('The ISSN has not a valid syntax.'));
    }
  }

  if (!valid_email_address($form_state['values']['medra_crossref_submit_fromemail'])) {
    form_set_error('medra_crossref_submit_fromemail', t('The email has not a valid syntax.'));
  }
}
