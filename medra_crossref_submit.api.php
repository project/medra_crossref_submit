<?php

/**
 * @file
 * DOI registration for Serial Article Work 2.0 provided by mEDRA.
 *
 * Date: 09 Sep 2019 12:39:23
 * File: medra_crossref_submit.api.php
 * Author: stefano.
 */

/**
 * Header.
 *
 * A valid ONIXDOISerialArticleWorkRegistrationMessage must contain one and
 * only one occurrence of the Header group.
 *
 * SC 09 Sep 2019 12:42:23 stefano.
 *
 * @param object $node
 *   The node object.
 *
 * @return array
 *   An array of header element.
 *   QName          Type     Use       Annotation
 *   MessageNumber  integer  optional  Sequence number assigned to messages sent in a series, to enable the receiver to check against gaps and duplicates.
 *   MessageRepeat  integer  optional  A number which distinguishes any repeat transmissions of a message. The original is numbered 1,andrepeats are numbered 2, 3 etc.
 *   MessageNote    string   optional  Free text giving additional information about the message.
 */
function hook_medra_crossref_submit_header($node) {
  return array(
    'header' => array(
      'MessageNumber' => 0,
      'MessageRepeat' => 1,
      'MessageNote' => '',
    ),
  );
}

/**
 * DOISerialArticleWork.
 *
 * Every ONIXDOISerialArticleWorkRegistrationMessage must contain one or more
 * occurrences of the DOISerialArticleWork group.
 *
 * SC 10 Sep 2019 18:18:20 stefano.
 *
 * @param object $node
 *   The node object.
 *
 * @return array
 *   An array of element.
 *   QName             Type     Use       Annotation
 *   NotificationType  Values   required  This element may only take the values specified in the datatype column: 06 indicates the first registration of a DOI; 07 indicates a subsequent update of the DOI metadata.
 *   DOI               string   required  The DOI assigned to the article, in the form “10.prefix/suffix". Example: 10.1472/abdc.
 *   DOIWebsiteLink    uri      required  URI of the resource that the registrant wants to associate to the DOI. To be used in order to associate one single resolution to the DOI (typically a web page).
 *   Collection        m_array  optional  group to enable Crossref Similarity Check service for antiplagiarism. The <Collection> element must carry the mandatory attribute property. For the antiplagiarism service, the attribute property must have the following value: crawler-based.
 *     Item            array    required  Metadata group including the crawlable URL pointing to the full-text article to be sent to Crossref in order to enable Similarity Check service for antiplagiarism. The <Item> element must carry the attribute crawler. The attribute crawler must have the value: iParadigms.
 *       Resource      uri      required  Crawlable URL pointing to the full-text article to be sent to Crossref in order to enable Similarity Check service.
 *   WorkIdentifier    m_array  optional  Group of elements that allows to specify one or more identifiers associated to the article (in addition to the DOI code).
 *     WorkIDType      string   required  The type of identifier associated to the article.
 *     IDValue         string   required  Value of the identifier.
 */
function hook_medra_crossref_submit_doiserialarticlework($node) {
  return array(
    'doiserialarticlework' => array(
      'NotificationType' => '06',
      // CR REQUIREMENTS: the length of the value specified in the <DOI> element
      // must be between 6 and 2048 characters.
      'DOI' => '10.1472/abdc',
      // CR requirements: the value of the <DOIWebsiteLink> element must be a
      // valid URI and have a length between 1 and 2048 characters.
      'DOIWebsiteLink' => 'https://example.org/node/1',
      'Collection' => array(
        array('Item' => array('Resource' => 'https://example.org/node/2')),
        array('Item' => array('Resource' => 'https://example.org/node/3')),
      ),
      // DOISerialArticleWork/DOIResolution - Optional metadata group to enable
      // the Multiple Resolution service (Multiple Resolution). It associates
      // multiple resolutions (multiple URIs) to the DOI. For the specifications
      // for the use of the DOIResolution group, please refer to the separate
      // documentation doi: 10.1392/ONIX_DOI_MR available on mEDRA Website.
      /* 'RegistrantName' => 'Foo', The same of Header/FromCompany */
      /* 'RegistrationAuthority' => 'mEDRA', */
      'WorkIdentifier' => array(
        // 01:
        // 11:
        array(
          'WorkIDType' => '01',
          'IDValue' => '',
        ),
        array(
          'WorkIDType' => '11',
          'IDValue' => '',
        ),
      ),
    ),
  );
}

/* function hook_medra_crossref_submit_doiresolution($node) */

/**
 * SerialWork.
 *
 * Every DOISerialArticleWork record must contain one SerialWork group wit The
 * message structure requires that the title (mandatory) and the publishe
 * expressed at "work” level. The inclusion of any identifier at "work" level
 * is optional.
 *
 * SC 12 Sep 2019 10:47:26 stefano.
 *
 * @return array
 *   An array of element.
 *   QName                  Type     Use       Annotation
 *   WorkIdentifier         m_array  optional  Group of elements that allows to specify one or more identifiers of the journal.
 *     WorkIDType           string   required  Identifier type. WARNING: In ONIX 2.0 for Serial Article Work multiple identifier types (01- Proprietary; 06- DOI, 08 CODEN) may be included. CR Recommendations: only the CODEN identifier (<WorkIDType> with value 08) is sent to CR
 *     IDValue              string   required  Identifier value. CR requirements: maximum length of six characters.
 *   Title                  m_array  required  Group of elements that allows to describe the title of the journal.
 *     TitleType            string   required  Title type. CR requirements: at least one Title group with child element <TitleType> equal to 01 (distinctiven title) must be present. See code lists sheet.
 *     TitleText            string   required  String of the publication title.
 *     Subtitle             string   optional  Publication subtitle.
 *   ImprintName            string   optional  Publishing brand responsible for the publication of the journal.
 *   Publisher              m_array  optional  Group of elements that allows to specify the name of the publisher and of any co-publishers of the journal.
 *     PublishingRole       string   required  Publisher’s role (publisher or co-publisher of the publication). See code lists sheet.
 *     PublisherIdentifier  m_array  optional  Group of elements that allows to specify one or more identifier associated to the publisher.
 *       PublisherIDType    string   required  Type of publisher identifier. Values: 01 (proprietary), 16 (ISNI).
 *       IDTypeName         string   optional  Element that allows to specify a proprietary identifier schema. This element must occur only if <PublisherIDType> value is 01 (proprietary).
 *       IDValue            string   required  Identifier value.
 *     PublisherName        string   required  Publisher’s name.
 *   CountryOfPublication   string   required  Country of publication, in the two-letter ISO 3166-1 format. For the full list of the values see the Onix 91 list.
 */
function hook_medra_crossref_submit_medra_crossref_submit_serialwork() {
  return array(
    'serialwork' => array(
      'WorkIdentifier' => array(
        array(
          'WorkIDType' => '',
          'IDValue' => '',
        ),
        array(
          'WorkIDType' => '',
          'IDValue' => '',
        ),
      ),
      'Title' => array(
        array(
          'TitleType' => '',
          'TitleText' => '',
          'Subtitle' => '',
        ),
        array(
          'TitleType' => '',
          'TitleText' => '',
          'Subtitle' => '',
        ),
      ),
      'ImprintName' => '',
      'Publisher' => array(
        array(
          'PublishingRole' => '',
          'PublisherIdentifier' => array(
            array(
              'PublisherIDType' => '01',
              'IDTypeName' => '',
              'IDValue' => '',
            ),
          ),
          'PublisherName' => '',
        ),
      ),
      'CountryOfPublication' => 'IT',
    ),
  );
}

/**
 * SerialVersion.
 *
 * Every DOISerialArticleWork record must contain at least one SerialVersion
 * group within the SerialPublication group. The SerialVersion group allows to
 * specify the identifiers of the journal (including the ISSN, mandatory for CR)
 * and to describe the publication format.
 *
 * SC 14 Sep 2019 13:17:51 stefano.
 *
 * @return array
 *   An multiple array of element.
 *   QName                  Type     Use       Annotation
 *   ProductIdentifier      m_array  required  Group of elements that allows to specify the identifiers of the journal.
 *     ProductIDType        string   required  Journal identififer. 07 (ISSN), 06 (DOI).
 *     IDValue              string   required  Identifier value.
 *   ProductForm            string   required  Journal format in which the article is published. Use this element to specify whether the journal is published in paper, electronic form or on CD-ROM. see code lists sheet.
 *   EpubFormat             string   optional  Electronic publication file format. It can only occur when <ProductForm> has JD value (Electronic journal, online). List 11.
 *   EpubFormatVersion      string   optional  Electronic publication version number. It can occur only if when the <EpubFormat> element is present. Max 10 char.
 *   EpubFormatDescription  string   optional  Text field that allows to further describe the features of the electronic publication. It can occur only if <ProductForm> has JD value but does not require the <EpubFormat> element to be present. Max 200 char.
 */
function hook_medra_crossref_submit_serialversion() {
  return array(
    'serialversion' => array(
      array(
        'ProductIdentifier' => array(
          array(
            'ProductIDType' => '07',
            'IDValue' => '',
          ),
        ),
        'ProductForm' => '',
        'EpubFormat' => '',
        'EpubFormatVersion' => '',
        'EpubFormatDescription' => '',
      ),
    ),
  );
}

/**
 * JournalIssue.
 *
 * Every DOISerialArticleWork record must contain at least one SerialIssue group
 * with the data concerning the journal issue (issue number, issue date, etc.).
 *
 * SC 16 Sep 2019 10:07:04 stefano.
 *
 * @param object $node
 *   The node object.
 *
 * @return array
 *   An multiple array of JournalIssue element.
 *   QName                    Type     Use       Annotation
 *   JournalVolumeNumber      integer  optional  Volume number, in which the journal appears. Max 15 digits.
 *   JournalIssueNumber       integer  optional  Journal number. Max 15 digits.
 *   JournalIssueDesignation  char     optional  Other journal indicators (where the journal number or the volume number is not available) in free-text format. Max 15 char.
 *   JournalIssueDate         array    required  Group of elements that allows to specify the issue date of the journal.
 *     DateFormat             string   required  Publication date format.
 *     Date                   string   required  Journal publication date.
 */
function hook_medra_crossref_submit_journalissue($node) {
  return array(
    'journalissue' => array(
      array(
        'JournalVolumeNumber' => 0,
        'JournalIssueNumber'  => 0,
        'JournalIssueDesignation' => '',
        'JournalIssueDate' => array(
          'DateFormat' => '00',
          'Date' => '20190101',
        ),
      ),
    ),
  );
}

/**
 * ContentItem.
 *
 * Every DOISerialArticleWork record must contain one and only one occurrence of
 * the ContentItem group with the descriptive metadata of the article to which
 * the DOI is assigned.
 *
 * SC 16 Sep 2019 12:08:33 stefano.
 *
 * @param object $node
 *   The node object.
 *
 * @return array
 *   An array of ContentItem element.
 *   QName                          Type     Use       Annotation
 *   SequenceNumber                 integer  optional  Number that allows to specify the location of the article within the publication table of contents.
 *   TextItem                       array    optional  Group of elements that allows to specify the type of textual content of the article and the page numbering.
 *     TextItemType                 string   required  Type of textual content of the article. See code list sheet.
 *     PageRun                      m_array  optional  Group of elements that allows to specify a run of contiguous pages on which the article appears.
 *       FirstPageNumber            string   required  Number of the first page of the article (expressed in Arabic digits, roman numerals, or as alphanumeric string). Max 15 char.
 *       LastPageNumber             string   optional  Number of the last page of the article (expressed in Arabic digits, roman numerals, or as alphanumeric string). Max 15 char.
 *     NumberOfPages                integer  optional  Overall number of pages of the article expressed as integer number. Max 6 digit.
 *   Title                          m_array  required  Group of elements that allows to specify the article title.
 *     TitleType                    string   required  CR Requirements: at least one Title group with child element <TitleType> equal to 01 must be present. No other values are allowed for the TitleType element.
 *     TitleText                    string   required  Article title.
 *     Subtitle                     string   optional  Article subtitle.
 *   Contributor                    m_array  optional  Repeatable group of elements that allows to describe, in every one of its occurrences, each author of the article and any other contributors (e.g. translator, editor, etc.).
 *     SequenceNumber               string   optional  Contributors' counter. CR requirements: the first author must always be provided, i.e. there must be one <SequenceNumber> element equal to 1, 01, or 001.
 *     ContributorRole              string   required  Role of the contributor (author, translator, editor, etc.).
 *     NameIdentifier               m_array  required  Group of elements that allows to specify one or more identifier associated to the contributor. See code lists.
 *       NameIDType                 string   required  Type of contributor identifier. Values: 01 (proprietary), 16 (ISNI), 21 (ORCID).
 *       IDTypeName                 string   optional  Element that allows to specify a proprietary identifier schema. This element must occur only if <NameIDType> value is 01 (proprietary). Max 50 char.
 *       IDValue                    string   required  Identifier value. WARNING: group of elements not present in previous versions of ONIX for DOI schema. ORCID ID must be expressed in the following form: http://orcid.org/xxxx-xxxx-xxxx-xxxx. Only the first. <NameIdentifier> group with child element <NameIDType> equal to 21 (ORCID ID) is sent to Crossref.
 *     NamesBeforeKey               string   optional  First name of the author (or of other contributors). It should be used in combination with KeyNames. Max 35 char.
 *     KeyNames                     string   optional  Last name of the author (or of other contributors). It should be used in combination with NamesBeforeKey. Max 35 char.
 *     ProfessionalAffiliation      m_array  optional  Group of elements that allows to describe the contributor's professional position or affiliation (e.g. University affiliation). Repeatable to indicate affiliations to multiple organizations.
 *       ProfessionalPosition       string   optional  Professional position of the contributor at the time of his/her participation in the article. Max 100 char.
 *       Affiliation                string   optional  Name of the organization to which the contributor is affiliated. Max 512 char.
 *     CorporateName                string   optional  To be used when the Contributor is not a physical person, but an organization or a company. Max 511 char.
 *     BiographicalNote             string   optional  Biographical note about the contributor. Max 500 char.
 *     UnnamedPersons               string   optional  To be used when the Contributor is anonymous or unknown.
 *     NoContributor                string   optional  To be used if no Contributor groups are present, to confirm that the article has no contributor. The use of this element allows to make sure that no loss of information in relation to the contributor has occurred during the transmission of the xml message.
 *   Language                       m_array  optional  Group of elements that allows to specify the text language or languages.
 *     LanguageRole                 string   required  Role of language with respect to the article. CR Recommendations: only the value 01 (text language) is allowed.
 *     LanguageCode                 string   required  Text language, in the three-letter ISO 639-2/B format. See code lists.
 *   MainSubject                    m_array  optional  Group of elements that allows to classify the article according to one or more among the main subject classification schemes (BIC, BISAC, etc.).
 *     MainSubjectSchemeIdentifier  string   required  Subject classification scheme used to classify the article. See ONIX 26 list.
 *     SubjectSchemeVersion         string   optional  Subject classification scheme version. Max 10 char.
 *     SubjectCode                  string   optional  Subject classification code taken from the selected scheme. Mandatory in the absence of <SubjectHeadingText> within the <MainSubject> group. Max 20 char.
 *     SubjectHeadingText           string   optional  Textual label of the subject classification code. Mandatory in the absence of <SubjectCode> within the <MainSubject> group. Max 100 char.
 *   Subject                        m_array  optional  Group of elements that allows to classify the article according to one or more additional schemes with respect to the main subject classification code, including proprietary classification systems used by the registrant.
 *     SubjectSchemeIdentifier      string   required  Subject classification scheme used to classify the article. See ONIX 27 list.
 *     SubjectSchemeName            string   optional  Subject classification scheme name. To be used when <SubjectSchemeIdentifier> has value 24 (proprietary scheme).
 *     SubjectSchemeVersion         string   optional  Subject classification scheme version. Max 10 char.
 *     SubjectCode                  string   optional  Subject classification code taken from the selected scheme. Mandatory in the absence of <SubjectHeadingText> within the <Subject> group. Max 20 char.
 *     SubjectHeadingText           string   optional  Textual label of the subject classification code. Mandatory in the absence of <SubjectCode> within the <Subject> group. Max 100 char.
 *   AudienceCode                   array    optional  Element that allows to specify the audience or readership for whom the publication is intended. ONIX 28 list.
 *   OtherText                      m_array  optional  Group of elements that allows to associate one or more texts related to the article (e.g. table of context, abstract, etc.).
 *     TextTypeCode                 string   required  Type of text associated to the article. ONIX 33 list.
 *     Text                         string   required  Text associated to the article.
 *   PublicationDate                string   required  Article publication date, in the publication format to which the DOI registration applies. 8, 6, or 4 digits: YYYYMMDD, YYYYMM, YYYY.
 *   CopyrightStatement             m_array  optional  Group of elements that allows to specify information on publication copyright owners (copyright year and copyright owner).
 *     CopyrightYear                string   required  Copyight year. 4 characters: YYYY.
 *     CopyrightOwner               m_array  optional  Group of elements that allows to specify the ownership of the publication copyright. At least one occurrence of CopyirghtOwner is mandatory within the CopyrightStatement group. Every CopyrightOwner group must contain, alternatively, a PersonName element or a CorporateName element, depending on whether the copyright owner is a physical person or an organization, such as for example the publisher.
 *       PersonName                 array    optional  Name of the copyright owner, to be used when the owner is a physical person.
 *       CorporateName              array    optional  Name of the copyright owner, to be used when the owner is an organization.
 *   RelatedWork                    m_array  optional  Group of elements that allows to specify, in every one of its occurrences, the relationship to a work linked to the article, specifying the type of relationship..
 *     RelationCode                 array    optional  Type of relationship between the article and the work. See code lists sheet.
 *     WorkIdentifier               m_array  optional  Group of elements that allows to specify one or more identifiers of the work linked to the article.
 *       WorkIDType                 string   required  Work identifier type. See code lists sheet.
 *       IDValue                    string   required  Identifier value.
 *   RelatedProduct                 m_array  optional  Group of elements that allows to specify, in every one of its occurrences, the relationship to a publication (manifestation) linked to the article, specifying the type of relationship..
 *     RelationCode                 array    optional  Type of relationship between the article and the publication. See code lists sheet.
 *     ProductIdentifier            m_array  optional  Group of elements that allows to specify one or more identifiers of the publication linked to the article.
 *       ProductIDType              string   required  Publication identifier type. Value: 01 (Proprietary), 02 ISBN-10, 03 EAN-13, 06 DOI, 10 SICI.
 *       IDValue                    string   required  Identifier value.
 */
function hook_medra_crossref_submit_contentitem($node) {
  return array(
    'contentitem' => array(
      'SequenceNumber' => 0,
      'TextItem' => array(
        'TextItemType' => '01',
        'PageRun' => array(
          array(
            'FirstPageNumber' => '',
            'LastPageNumber' => '',
          ),
        ),
        'NumberOfPages' => 0,
      ),
      'Title' => array(
        array(
          'TitleType' => '01',
          'TitleText' => '',
          'Subtitle' => '',
        ),
      ),
      'Contributor' => array(
        array(
          'SequenceNumber' => '001',
          'ContributorRole' => '',
          'NameIdentifier' => array(
            array(
              'NameIDType' => '01',
              'IDTypeName' => '',
              'IDValue' => '',
            ),
          ),
          'NamesBeforeKey' => '',
          'KeyNames' => '',
          'ProfessionalAffiliation' => array(
            array(
              'ProfessionalPosition' => '',
              'Affiliation' => '',
            ),
          ),
          'CorporateName' => '',
          'BiographicalNote' => '',
          'UnnamedPersons' => '01',
          'NoContributor' => '',
        ),
      ),
      'Language' => array(
        array(
          'LanguageRole' => '01',
          'LanguageCode' => 'eng',
        ),
      ),
      'MainSubject' => array(
        array(
          'MainSubjectSchemeIdentifier' => '01',
          'SubjectSchemeVersion' => '',
          'SubjectCode' => '',
          'SubjectHeadingText' => '',
        ),
      ),
      'Subject' => array(
        array(
          'SubjectSchemeIdentifier' => '01',
          'SubjectSchemeName' => '',
          'SubjectSchemeVersion' => '',
          'SubjectCode' => '',
          'SubjectHeadingText' => '',
        ),
      ),
      'AudienceCode' => array('01', '02', '04'),
      'OtherText' => array(
        array(
          'TextTypeCode' => '02',
          'Text' => '',
        ),
      ),
      'PublicationDate' => '20190101',
      'CopyrightStatement' => array(
        array(
          'CopyrightYear' => '2019',
          'CopyrightOwner' => array(
            array(
              'PersonName' => array(),
              'CorporateName' => array(),
            ),
          ),
        ),
      ),
      'RelatedWork' => array(
        array(
          'RelationCode' => array(),
          'WorkIdentifier' => array(
            array(
              'WorkIDType' => '06',
              'IDValue' => '',
            ),
          ),
        ),
      ),
      'RelatedProduct' => array(
        array(
          'RelationCode' => array(),
          'ProductIdentifier' => array(
            array(
              'ProductIDType' => '06',
              'IDValue' => '',
            ),
          ),
        ),
      ),
    ),
  );
}

/**
 * CitationList.
 *
 * <cl:CitationList> record (defined in the schema
 * http://www.medra.org/schema/onix/DOIMetadata/2.0/ONIX_DOICitations_2.0.xsd)
 * and included in the ONIX DOI schema via the alias "cl:") which allows to
 * insert the list of citations (or bibliographic references) of the article
 * for which the DOI registration/update is carried out. The <CitationList>
 * record may be included within the ContentItem group in the
 * DOISerialArticleWork record and deposited at the time of DOI
 * registration/update or afterwards with a separate registration message.
 * Every CitationList group may contain a) the descriptive metadata of the
 * cited publication (article or monograph), b) the DOI of the cited
 * publication c) both d) the <UnstructuredCitations> element.
 *
 * SC 18 Sep 2019 12:01:50 stefano.
 *
 * @param object $node
 *   The node object.
 *
 * @return array
 *   An array of ContentItem element.
 *   QName                      Type     Use       Annotation
 *   ArticleCitation            m_array  optional  Repeatable group of elements that allows to enter, in each one of its occurrences, the metadata of a citation. Each occurrence of <ArticleCitation> must be associated with the key attribute, to allow for the identification of the citation and the correct processing within the system. The key attribute must be created by the user according to the syntax rules as specified in the Attributes sheet.
 *     ISSN                     string   optional  ISSN of the journal in which the cited article appears. The <ISSN> element may be associated, optionally, with the media_type attribute.
 *     media-type               string   optional  It allows to specify the publication format (paper or electronic of the cited publication). Value: "print" or "electronic".
 *     JournalTitle             string   required  Title of the publication in which the article appears.
 *     AuthorName               string   required  Name of the main author of the cited article. The <AuthorName> element must be mandatorily associated with the referent-type attribute, to specify whether the author is a physical person or an organization.
 *     referent-type            string   required  The referent-type attribute is mandatory and must be used to specify whether the cited author is a physical person or an organization. Value: "person" or "corporate".
 *     JournalVolumeNumber      string   optional  Number of the volume, in which the journal appears. Recommendations: the <JournalVolumeNumber> element is only sent to Crossref if its value length is smaller or equal to 15 characters.
 *     JournalIssueNumber       string   optional  Number of the cited publication. Recommendations: the <JournalIssueNumber> element is only sent to Crossref if its value length is smaller or equal to 15 characters.
 *     JournalIssueDesignation  string   optional  Other designators of the cited publication (in the absence of the journal number or the volume number) in free-text format.
 *     FirstPageNumber          string   req/opt   Number of the first page of the cited publication (article or monograph). CR Recommendations: the <FirstPageNumber> element is only sent to Crossref if its value length is smaller or equal to 15 characters. Required for article, optional for monograph.
 *     JournalIssueDate         array    optional  Group of elements that allows to specify the issue date of the cited publication.
 *       DateFormat             string   required  Date format. All date formats are allowed for the transfer to CR. CR Recommendations: to optimally exploit the CR service it is recommended to use the format YYYY (value 05 in code list 55) or the format YYYYYYYY (value 11 in code list 55). ONIX 55 list.
 *       Date                   string   required  Date string, in the format specified in the DateFormat element.
 *     DOI                      string   req/opt   DOI of the cited article, in the form "10.prefix/suffix". Example: 10.1472/abdc. CR Requirements: the <DOI> element value length must be between 6 and 2048 characters. Requiredo only for DOI (case 3).
 *     ArticleTitle             string   optional  Title of cited article.
 *     BookTitle                string   required  Title of the cited monograph.
 *     ISBN                     string   optional  ISBN of the cited monograph, without hyphens.
 *     TitleOfSeries            string   optional  Title of the series in which the cited monograph appears.
 *     NumberWithinSeries       string   optional  Number of the monograph within the series CR Recommendations: the <NumberWithinSeries> is only sent to Crossref if its value length is smaller or equal to 15 characters.
 *     EditionNumber            string   optional  Edition number of the cited monograph. CR Recommendations: the <EditionNumber> element is only sent to Crossref if its value length is smaller or equal to 15 characters.
 *     PublicationDate          array    required  Publication date of the cited monograph. CR Recommendations: use the YYYY or the YYYY-YYYY format.
 *     ComponentNumber          string   optional  Number of a unit of content, section, chapter, within the cited monograph. CR Recommendations: the <ComponentNumber> element is only sent to Crossref if its value length is smaller or equal to 50 characters.
 *     UnstructuredCitation     string   required  Element which allows to enter a citation as free-text field. The text string may contain the following face-markup (if any): (b) bold, (i) italic, (u) underline, (ovl) over-line, (sup) superscript, (sub) subscript, (scp) small caps, (tt) typewriter text.
 *     key                      string   required  The key attribute is mandatory for each occurrence of <ArticleCitation>, to allow for the identification of the citation and the correct processing within the system. The key attribute is an alphanumeric string. The string integrates within it the DOI code (for example the DOI of the article from which the citations are taken) plus a numeric reference to the citation contained in <ArticleCitation>, according to the syntax: "10.prefix/suffix_ref + variable-length number". Examples: 10.1472/abc_ref01, 10.1472/abc_ref02, 10.1472/abc_ref03, ecc.
 */
function hook_medra_crossref_submit_citationlist($node) {
  return array(
    'citationlist' => array(
      'ArticleCitation' => array(
        // Use case 1: Citation of an article.
        array(
          'ISSN' => '',
          'media-type' => 'print',
          'JournalTitle' => '',
          'AuthorName' => '',
          'referent-type' => '',
          'JournalVolumeNumber' => '',
          'JournalIssueNumber' => '',
          'JournalIssueDesignation' => '',
          'FirstPageNumber' => '',
          'JournalIssueDate' => array(
            'DateFormat' => '',
            'Date' => '',
          ),
          'DOI' => '',
          'ArticleTitle' => '',
          'key' => '',
        ),
        // Use case 2: Citation of a monograph.
        array(
          'BookTitle' => '',
          'ISBN' => '',
          'AuthorName' => '',
          'referent-type' => '',
          'ISSN' => '',
          'media-type' => 'electronic',
          'TitleOfSeries' => '',
          'NumberWithinSeries' => '',
          'EditionNumber' => '',
          'PublicationDate' => array(),
          'ComponentNumber' => '',
          'FirstPageNumber' => '',
          'DOI' => '',
          'key' => '',
        ),
        // Use case 3: DOI citation.
        array(
          'DOI' => '',
          'key' => '',
        ),
        // Use case 4: Citation of an unstructured format.
        array(
          'UnstructuredCitation' => '',
          'key' => '',
        ),
      ),
    ),
  );
}
