<?php

/**
 * @file
 * Web service for DOI registration provided by mEDRA.
 *
 * Date: 20 Sep 2019 10:26:23
 * File: medra_crossref_submit_ws.api.php
 * Author: stefano.
 */

/**
 * Update from the XML report.
 *
 * When the request is processed and completed, the system generates a report
 * with the description of the request outcome. If the user chooses to receive
 * the report via HTTP-callback (by specifying properly the
 * NotificationResponse element) the system produces an XML document.
 *
 * SC 25 Oct 2019 18:46:30 stefano.
 *
 * @param array $results
 *   A bidimensional array:
 *   array
 *     doi    => the DOI [string]
 *     status => the processing outcome of a single record [bool].
 */
function hook_medra_crossref_submit_ws_request_processed($results) {
  foreach ($results as $result) {
    db_update('yout_table')
      ->fields(array(
        'status' => ($result->status),
      ))
      ->condition('doi', $result->doi)
      ->execute();
  }
}
