<?php

/**
 * @file
 * Web service for DOI registration provided by mEDRA.
 *
 * Class to interface with the web service.
 *
 * Based to ojs/master/plugins/importexport/medra/classes/MedraWebservice.inc.php.
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class MedraWebservice
 * @ingroup plugins_importexport_medra_classes
 *
 * @brief A wrapper for the mEDRA web service 2.0.
 *
 *
 * Date: 20 Sep 2019 16:44:51
 * File: medra_crossref_submit_ws.class.inc
 * Author: stefano.
 */

/**
 * Web Service interface.
 *
 * SC 20 Sep 2019 17:25:40 stefano.
 */
class MedraCrossrefSubmitWs {

  /**
   * HTTP authentication credentials.
   *
   * @var string
   */
  private $auth;

  /**
   * The mEDRA web service endpoint.
   *
   * @var string
   */
  private $endpoint;

  /**
   * The preferred language to use for communications.
   *
   * SC 02 Oct 2019 11:07:50 stefano.
   *
   * @var string
   */
  private $lang;

  /**
   * Constructor.
   *
   * SC 20 Sep 2019 18:46:47 stefano.
   *
   * @param string $endpoint
   *   The mEDRA web service endpoint.
   * @param string $login
   *   The user name.
   * @param string $password
   *   The user password.
   */
  public function __construct($endpoint, $login, $password) {
    $this->endpoint = $endpoint;
    $this->auth = $login . ':' . $password;

    // Language.
    //
    // SC 02 Oct 2019 11:25:24 stefano.
    //
    global $language;

    switch ($language->language) {
      case 'it';
        $this->lang = 'ita';
        break;

      case 'de';
        $this->lang = 'ger';
        break;

      default;
        $this->lang = 'eng';
        break;
    }
  }

  //
  // Public Web Service Actions.
  //

  /**
   * The mEDRA upload operation.
   *
   * SC 20 Sep 2019 18:51:13 stefano.
   *
   * @param string $xml
   *   The data.
   *
   * @return string
   *   The upload result.
   */
  public function deposit($xml) {
    $attachmentId = $this->getContentId('metadata');
    $attachment = array($attachmentId => $xml);
    return $this->doRequest('deposit', $attachmentId, $attachment);
  }

  /**
   * The mEDRA/Crossref citation upload operation.
   *
   * SC 20 Sep 2019 18:51:13 stefano.
   *
   * @param string $xml
   *   The data.
   *
   * @return string
   *   The upload result.
   */
  public function citationDeposit($xml) {
    $attachmentId = $this->getContentId('metadata');
    $attachment = array($attachmentId => $xml);
    return $this->doRequest('citationDeposit', $attachmentId, $attachment);
  }

  /**
   * The Crossref query operation.
   *
   * SC 20 Sep 2019 18:56:21 stefano.
   *
   * @param string $xml
   *   The data.
   *
   * @return string
   *   The query result.
   */
  public function query($xml) {
    $attachmentId = $this->getContentId('metadata');
    $attachment = array($attachmentId => $xml);
    return $this->doRequest('query', $attachmentId, $attachment);
  }

  /*
   * Internal helper methods.
   */

  /**
   * Do the actual web service request.
   *
   * SC 20 Sep 2019 19:25:08 stefano.
   *
   * @param string $action
   *   The action.
   * @param string $arg
   *   The data.
   * @param array $attachment
   *   The XML.
   *
   * @return array
   *   An array with
   *   - success => true or false
   *   - message => the error if
   *   True for success, an error message otherwise.
   */
  private function doRequest($action, $arg, $attachment = array()) {
    // Build the multipart SOAP message from scratch.
    $soapMessage = <<<SOAPM
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:med="http://www.medra.org">
  <soapenv:Header/>
  <soapenv:Body>
    <med:$action>
      <med:accessMode>01</med:accessMode>
      <med:language>$this->lang</med:language>
      <med:contentID>$arg</med:contentID>
    </med:$action>
  </soapenv:Body>
</soapenv:Envelope>
SOAPM;

    $soapMessageId = $this->getContentId($action);
    if ($attachment) {
      assert(count($attachment) == 1);
      $request =
        "--MIME_boundary\r\n" .
        $this->getMimePart($soapMessageId, $soapMessage) .
        "--MIME_boundary\r\n" .
        $this->getMimePart(key($attachment), current($attachment)) .
        "--MIME_boundary--\r\n";
      $contentType = 'multipart/related; type="text/xml"; boundary="MIME_boundary"';
    }
    else {
      $request = $soapMessage;
      $contentType = 'text/xml';
    }

    // Prepare HTTP session.
    $curlCh = curl_init();
    curl_setopt($curlCh, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curlCh, CURLOPT_POST, TRUE);

    // Set up basic authentication.
    curl_setopt($curlCh, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curlCh, CURLOPT_USERPWD, $this->auth);

    // Set up SSL.
    curl_setopt($curlCh, CURLOPT_SSL_VERIFYPEER, FALSE);

    // Make SOAP request.
    curl_setopt($curlCh, CURLOPT_URL, $this->endpoint);
    $extraHeaders = array(
      'SOAPAction: "' . $action . '"',
      'Content-Type: ' . $contentType,
      'UserAgent: Drupal (+http://drupal.org/)',
    );
    curl_setopt($curlCh, CURLOPT_HTTPHEADER, $extraHeaders);
    curl_setopt($curlCh, CURLOPT_POSTFIELDS, $request);

    $result = TRUE;
    $response = curl_exec($curlCh);

    // We do not localize our error messages as they are all
    // fatal errors anyway and must be analyzed by technical staff.
    if ($response === FALSE) {
      $result = FALSE;
      $submission_id = '';
      drupal_set_message(t('<strong>mEDRA/Crossref submit</strong>: Expected string response.'), 'error', FALSE);
    }

    if ($result === TRUE && ($status = curl_getinfo($curlCh, CURLINFO_HTTP_CODE)) != MEDRA_CROSSREF_SUBMIT_WS_RESPONSE_OK) {
      $result = FALSE;
      $submission_id = '';
      drupal_set_message(t('<strong>mEDRA/Crossref submit</strong>: Expected @response_ok response code, got @status instead.', array(
        '@response_ok' => MEDRA_CROSSREF_SUBMIT_WS_RESPONSE_OK,
        '@status' => $status,
      )), 'error', FALSE);
    }

    curl_close($curlCh);

    // Check SOAP response by simple string manipulation rather
    // than instantiating a DOM.
    if (is_string($response)) {
      $xml_body = simplexml_load_string($response)->children('SOAP-ENV', TRUE)->Body->children('');

      switch ($action) {
        case 'deposit':
        case 'citationDeposit':
          $xml_body_content = $xml_body->depositUploadResponse;
          /* $messagenumber = isset($xml_body_content->messageNumber) ? (string) $xml_body_content->messageNumber : NULL; */
          break;

        case 'query':
          $xml_body_content = $xml_body->queryUploadResponse;
          /* $messagenumber = isset($xml_body_content->messageReferenceNumber) ? (string) (string) $xml_body_content->messageNumber : NULL; */
          break;
      }

      $responseSubmissionId = (string) $xml_body_content->attributes()->contentID;

      if ($arg == $responseSubmissionId) {
        if ((string) $xml_body_content->statusCode == 'SUCCESS' && !(int) $xml_body_content->errorsNumber) {
          $result = TRUE;
          $submission_id = (!empty($xml_body_content->submissionID) ? (string) $xml_body_content->submissionID : '');

          if ((int) $xml_body_content->warningsNumber) {
            foreach ($xml_body_content->warning as $value) {
              drupal_set_message(t('<strong>mEDRA/Crossref submit</strong>: @code: @reference - @description', array(
                '@code' => (string) $value->code,
                '@reference' => (string) $value->reference,
                '@description' => (string) $value->description,
              )), 'warning', FALSE);
            }

            drupal_set_message(t('<strong>mEDRA/Crossref submit</strong>: XML upload successfully to mEDRA/Crossref, but some warnings occourred.'), 'status', FALSE);
          }
          else {
            drupal_set_message(t('<strong>mEDRA/Crossref submit</strong>: XML upload successfully to mEDRA/Crossref.'), 'status', FALSE);
          }
        }
        elseif ((string) $xml_body_content->statusCode == 'FAILURE' && (int) $xml_body_content->errorsNumber) {
          $result = FALSE;
          $submission_id = (!empty($xml_body_content->submissionID) ? $xml_body_content->submissionID : '');
          foreach ($xml_body_content->warning as $value) {
            drupal_set_message(t('<strong>mEDRA/Crossref submit</strong>: @code - @reference - @description', array(
              '@code' => (string) $value->code,
              '@reference' => (string) $value->reference,
              '@description' => (string) $value->description,
            )), 'error', FALSE);
          }
        }
        else {
          $result = NULL;
          $submission_id = (!empty($xml_body_content->submissionID) ? $xml_body_content->submissionID : '');
        }
      }
      elseif ((string) $xml_body_content->statusCode == 'FAILED') {
        $xml_curl_fault = $xml_body->Fault->children('');
        $result = FALSE;
        $submission_id = (!empty($xml_body_content->submissionID) ? $xml_body_content->submissionID : '');
        drupal_set_message(t('medra_crossref_submit_ws curl: @code - @description', array(
          '@code' => (string) $xml_curl_fault->faultcode,
          '@description' => (string) $xml_curl_fault->faultstring,
        )), 'error', FALSE);
      }
      else {
        $result = NULL;
        $submission_id = (!empty($xml_body_content->submissionID) ? $xml_body_content->submissionID : '');
      }
    }
    else {
      $result = FALSE;
      $submission_id = '';
      drupal_set_message(t('<strong>mEDRA/Crossref submit</strong>: Expected string response.'), 'error', FALSE);
    }

    return array(
      'status' => $result,
      'id' => $submission_id,
    );
  }

  /**
   * Create a mime part with the given content.
   *
   * SC 02 Oct 2019 15:10:38 stefano.
   *
   * @param string $contentId
   *   The unique ID.
   * @param string $content
   *   The data to seng.
   *
   * @return string
   *   The mime part.
   */
  private function getMimePart($contentId, $content) {
    return "Content-Type: text/xml; charset=utf-8\r\nContent-ID: <" . $contentId . ">\r\n\r\n" . $content . "\r\n";
  }

  /**
   * Create a globally unique MIME content ID.
   *
   * SC 02 Oct 2019 12:30:21 stefano.
   *
   * @param string $prefix
   *   A label.
   *
   * @return string
   *   A unique ID.
   */
  private function getContentId($prefix) {
    return $prefix . md5(uniqid()) . '@medra.org';
  }

}
